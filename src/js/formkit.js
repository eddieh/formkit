/*
 * formkit.js
 *
 * Author: Eddie Hillenbrand
 */

/* Focus the input element when the inset is clicked. This is only
 * needed for IE and older Opera browsers that don't support
 * 'pointer-events: none'
 */
!function ($) {
  var InputInset = function(element, options) {
    this.$element = $(element);
    this.$input = this.$element.parent().find('input');

    // setup for subsequent clicks
    this.$element.click($.proxy(function (e) { this.$input.focus(); }, this));

    // handle this click
    this.$input.focus();
  };

  $.fn.inputInset = function(option) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('inputinset'),
          options = typeof(option) == 'object' && option;

      if (!data)
        $this.data('inputinset', (data = new InputInset(this, options)));
      if (typeof(option) == 'string') data[option]();
    });
  };

  $.fn.inputInset.defaults = {
  };

  $.fn.inputInset.Constructor = InputInset;

  // we could use Modernizer...but we only need this one test
  function hasPointerEvents () {
    return document.documentElement.style.pointerEvents === '';
  }

  if (!hasPointerEvents()) {
    $(document).on(
      'click.inputinset.data-api',
      '.input-inset .inset',
      function(e) {
        var $this = $(this);
        if ($this.data('inputinset')) return;
        $this.inputInset($this.data());
      });
  }

}(window.jQuery);

!function ($) {
  var EMAIL_REGEX = /^[\w.%+-]+@[\w.-]+\.[A-Za-z]{2,4}$/;
  var ZIP_REGEX = /[0-9]{5}(-[0-9]{4})?/;

  var ValidityState = function () {
    this.badInput = false;
    this.customError = false;
    this.patternMismatch = false;
    this.invalidEmail = false;
    this.invalidPhone = false;
    this.invalidZip = false;
    this.rangeOverflow = false;
    this.rangeUnderflow = false;
    this.stepMismatch = false;
    this.tooLong = false;
    this.typeMismatch = false;
    this.valid = false;
    this.valueMissing = false;
    this.hasHadFocus = false;
  };

  var InputValidator = function(element, options) {
    this.$element = $(element);
    this.validity = new ValidityState();
    this.listen();
  };

  InputValidator.prototype.listen = function () {
    this.$element
      .on('focus', $.proxy(this.focus, this))
      .on('blur', $.proxy(this.blur, this))
      .on('keypress', $.proxy(this.keypress, this))
      .on('keyup', $.proxy(this.keyup, this));

    if (this.eventSupported('keydown')) {
      this.$element.on('keydown', $.proxy(this.keydown, this));
    }
  };

  InputValidator.prototype.eventSupported = function(eventName) {
    var isSupported = eventName in this.$element;
    if (!isSupported) {
      this.$element.setAttribute(eventName, 'return;');
      isSupported = typeof(this.$element[eventName] === 'function');
    }
    return isSupported;
  };

  InputValidator.prototype.keydown = function (e) {
  };

  InputValidator.prototype.keypress = function (e) {
  };

  InputValidator.prototype.keyup =  function (e) {
    this.validate();
    e.stopPropagation();
    e.preventDefault();
  };

  InputValidator.prototype.focus = function (e) {
    //this.focused = true;

    if (typeof(this.$element.data('phone')) == 'string')
      this.$element.val(this.originalNumber);
  };

  InputValidator.prototype.blur = function (e) {
    // this.focused = false;
    this.validity.hasHadFocus = true;
    this.validate();

    if (typeof(this.$element.data('phone')) == 'string') {
      this.$element.val(Phone.formatNumber(
        this.strippedNumber,
        this.countryCode));
    }
  };

  InputValidator.prototype.validate = function () {
    this.validity.valid = true;

    if (typeof(this.$element.data('required')) == 'string') {
      this.validity.valueMissing = this.$element.val() == '';
      this.validity.valid &= !this.validity.valueMissing;
    }

    if (typeof(this.$element.data('pattern')) == 'string') {
      if (!this.pattern)
        this.pattern = new RegExp(this.$element.data('pattern'));
      this.validity.patternMismatch = !this.pattern.test(this.$element.val());
      this.validity.valid &= !this.validity.patternMismatch;
    }

    if (typeof(this.$element.data('email')) == 'string') {
      this.validity.invalidEmail = !EMAIL_REGEX.test(this.$element.val());
      this.validity.valid &= !this.validity.invalidEmail;
    }

    if (typeof(this.$element.data('phone')) == 'string') {
      this.originalNumber = this.$element.val();
      this.strippedNumber = Phone.stripNumber(this.originalNumber);
      this.countryCode = Phone.inferCountry(this.strippedNumber);
      this.validity.invalidPhone =
        !Phone.possibleNumber(this.strippedNumber, this.countryCode);
      this.validity.valid &= !this.validity.invalidPhone;
    }

    if (typeof(this.$element.data('zip')) == 'string') {
      var zip = this.$element.val();
      if (!this.zipRequests) this.zipRequests = {};
      if (ZIP_REGEX.test(zip) && !this.zipRequests[zip]) {
        this.zipRequests[zip] =
          $.getJSON('http://zip.elevenbasetwo.com/v2/US/' + zip);
      }
      var req = this.zipRequests[zip];
      this.validity.invalidZip =
        $.isEmptyObject(req ? req.responseJSON : {});
      this.validity.valid &= !this.validity.invalidZip;
    }

    this[this.validity.valid ? 'valid' : 'invalid']();
  };

  InputValidator.prototype.invalid = function () {
    if (this.validity.hasHadFocus) {
      this.$element.removeClass('valid')
        .addClass('invalid')
        .parent()
        .find('.inset i')
        .removeClass('icon-ok')
        .removeClass('flag')
        .removeClass(typeof(this.countryCode) == 'string' ?
                     this.countryCode.toLowerCase() : '')
        .addClass('icon-remove');
    }
    this.$element.trigger('invalid.fk.inputvalidator');
  };

  InputValidator.prototype.valid = function () {
    if (this.validity.hasHadFocus) {
      var $insetIcon = this.$element.removeClass('invalid')
        .addClass('valid')
        .parent()
        .find('.inset i')
        .removeClass('icon-remove');

      if (typeof(this.$element.data('phone')) == 'string') {
        $insetIcon.addClass('flag ' + this.countryCode.toLowerCase());
      }
      else {
        $insetIcon.addClass('icon-ok');
      }
    }
    this.$element.trigger('valid.fk.inputvalidator');
  };

  $.fn.inputValidator = function(option) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('inputvalidator'),
          options = typeof(option) == 'object' && option;

      if (!data)
        $this.data('inputvalidator', (data = new InputValidator(this, options)));
      if (typeof(option) == 'string') data[option]();
    });
  };

  $.fn.inputValidator.defaults = {
  };

  $.fn.inputValidator.Constructor = InputValidator;

  $(document).on(
    'focus.inputvalidator.data-api',
    '[data-required],[data-pattern],[data-email],[data-phone],[data-zip]',
    function(e) {
      var $this = $(this);
      if ($this.data('inputvalidator')) return;
      $this.inputValidator($this.data());
    });

}(window.jQuery);
