var Phone = (function (metadata) {

  var DefaultCountry = 'US';

  var GENERAL_DESC = 1;
  var NATIONAL_NUMBER_PATTERN = 2;
  var POSSIBLE_NUMBER_PATTERN = 3;

  var NUMBER_FORMAT = 19;
  var INTL_NUMBER_FORMAT = 20;
  var PATTERN = 1;
  var FORMAT = 2;
  var LEADING_DIGITS_PATTERN = 3;

  var matchesEntirely = function(regex, str) {
    var matchedGroups = (typeof regex == 'string') ?
        str.match('^(?:' + regex + ')$') : str.match(regex);
    if (matchedGroups && matchedGroups[0].length == str.length) {
      return true;
    }
    return false;
  };

  var stripNumber = function (number) {
    return number.replace(/[^\+\d]|\s*/g, '');
  };

  var inferCountry = function (number) {
    var map = metadata.countryCodeToRegionCodeMap,
        cc1 = '',
        cc2 = '',
        cc3 = '';

    if (number.charAt(0) != '+') return DefaultCountry;

    cc1 += number.charAt(1);
    cc2 = cc1 + number.charAt(2);
    cc3 = cc2 + number.charAt(3);

    if (cc3 in map) {
      return map[cc3][0];
    } else {
      if (cc2 in map) {
        return map[cc2][0];
      }
      return map[cc1] ? map[cc1][0] : 'unknown';
    }
  };

  var possibleNumber = function (number, country) {
    country = country || DefaultCountry;

    try {
      var m = metadata.countryToMetadata[country],
          r1 = new RegExp(m[GENERAL_DESC][POSSIBLE_NUMBER_PATTERN]),
          r2 = new RegExp(m[GENERAL_DESC][NATIONAL_NUMBER_PATTERN]);
    } catch (e) { return false; }

    return r1.test(number) && r2.test(number);
  };

  var formatNumber = function (number, country) {
    country = country || DefaultCountry;

    try {
      var m = metadata.countryToMetadata[country],
          formats = m[INTL_NUMBER_FORMAT] || m[NUMBER_FORMAT],
          l = formats.length,
          f = '';
    } catch (e) { return number; }

    for (var i = 0; i < l; ++i) {
      f = formats[i];
      var s = f[LEADING_DIGITS_PATTERN] ? f[LEADING_DIGITS_PATTERN].length : 0;
      if (s == 0 || number.search(f[LEADING_DIGITS_PATTERN][s - 1]) == 0) {
        var p = f[PATTERN];
        if (matchesEntirely(p, number)) {
          break;
        }
      }
    }

    var pattern = new RegExp(f[PATTERN]);
    var rule = f[FORMAT];

    // rule string uses special replacement patterns
    var formatted = number.replace(pattern, rule);

    return formatted;
  };

  return {
    stripNumber: stripNumber,
    inferCountry: inferCountry,
    possibleNumber: possibleNumber,
    formatNumber: formatNumber
  };
}(i18n.phonenumbers.metadata));
