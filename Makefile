bundler:
	bundle install --path vendor/bundle
	bundle install --binstubs

bower:
	npm install bower
	node_modules/.bin/bower install

docs/assets/js:
	mkdir -p docs/assets/js

docs/assets/css:
	mkdir -p docs/assets/css

docs/assets/img:
	mkdir -p docs/assets/img

jquery: docs/assets/js
	cp vendor/bower/jquery/jquery.js docs/assets/js

bootstrap: docs/assets/js docs/assets/css docs/assets/img
	cp vendor/bower/bootstrap/css/bootstrap.css docs/assets/css
	cp vendor/bower/bootstrap/css/bootstrap-responsive.css docs/assets/css
	cp vendor/bower/bootstrap/img/*.png docs/assets/img
	cp vendor/bower/bootstrap/js/bootstrap.js docs/assets/js

docs/dist/js:
	mkdir -p docs/dist/js

docs/dist/css:
	mkdir -p docs/dist/css

docs/dist/img:
	mkdir -p docs/dist/img

.PHONY: docs
docs: docs/dist/js docs/dist/css docs/dist/img
	cp src/js/*.js docs/dist/js
	cp src/css/*.css docs/dist/css
	cp src/img/*.png docs/dist/img

serve:
	bin/jekyll serve --watch

build:
	bin/jekyll build
