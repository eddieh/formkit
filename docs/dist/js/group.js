!function ($) {
  var Group = function(element, options) {
    this.$element = $(element);
    this.options = $.extend({}, $.fn.group.defaults, options);

    this.$inputs = this.$element.find('input');
    this.$inputs.on('valid.fk.inputvalidator invalid.fk.inputvalidator',
                    $.proxy(this.validate, this));

    this.inputs = $.map(this.$inputs.get(), function (e) {
      var $input = $(e);
      if (!$input.data('inputvalidator'))
        $input.inputValidator($input.data());
      return $input.data('inputvalidator');
    });

    var submitSelector = this.$element.attr('data-group-submit'),
        resetSelector = this.$element.attr('data-group-reset');
    this.$submit = this.$element.find(submitSelector || '[type=submit]');
    this.$reset = this.$element.find(resetSelector || '[type=reset]');

    this.validate();
  };

  Group.prototype.validate = function () {
    var allValid = true;
    $.each(this.inputs, function (index, value) {
      if (!this.validity.valid) allValid = false;
    });
    this[allValid ? 'valid' : 'invalid']();
  };

  Group.prototype.invalid = function () {
    this.$submit.attr('disabled', 'disabled');
  };

  Group.prototype.valid = function () {
    this.$submit.removeAttr('disabled');
  };

  $.fn.group = function(option) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('group'),
          options = typeof(option) == 'object' && option;

      if (!data)
        $this.data('group', (data = new Group(this, options)));
      if (typeof(option) == 'string') data[option]();
    });
  };

  $.fn.group.defaults = {
  };

  $.fn.group.Constructor = Group;

  $(window).on('load', function () {
    $('[data-group]').each(function () {
      $(this).group();
    });
  });

}(window.jQuery);
